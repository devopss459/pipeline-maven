#!/bin/bash

echo "***************************"
echo "** Building jar ***********"
echo "***************************"

WORKSPACE=/home/ec2-user/devops/jenkins_home/workspace/pipeline-maven

docker run --rm  -v  $WORKSPACE/java_app:/app -v /root/.m2/:/root/.m2/ -w /app maven:3-alpine "$@"

