#!/bin/bash

echo maven-project > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $PASS >> /tmp/.auth

scp -i /opt/prod /tmp/.auth ec2-user@3.95.34.157:/tmp/.auth
scp -i /opt/prod ./jenkins/deploy/publish ec2-user@3.95.34.157:/tmp/publish
ssh -i /opt/prod ec2-user@3.95.34.157 "/tmp/publish"
